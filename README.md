# Cluster

Вопросы мне можно задать по почте a.trinogin@yandex.ru.  
Либо Козлову Константину Николаевичу.

## GCDM

https://sourceforge.net/projects/gcdm/

## Gemstat

https://sourceforge.net/projects/gemstat/

Лучше уточнять какая ветка акутальна на текущий момент времени, на момент моей работы нужно было работать с alpha28.

## Connect to cluster 

Для подключения к кластеру можно использовать обычный CLI, но рекомендую взять какой-нибудь SSH-клиент, 
для линуксовых машин хорошо подойдёт Remmina, для Windows удобно работать с gitBash (соотвественно, он должен быть
установлен в процессе установки git, либо доустановлен позже).

Команда подключения:
```bash
ssh -l your_login -i key_file_name login1.hpc.spbstu.ru
```
P.s. нужно помнить, что файл с ключом должен лежать в папке home/.ssh, либо в Users/User/.ssh (windows).

## File import/export

Чтобы отправить файлы на кластер или получить их можно пользоваться утилитой scp (see manuals)[^1]:

**user_home** - папка home текущего пользователя.

1. Import example.
```bash
scp login1.hpc.spbstu.ru:/home/nilmbb/**user_home**/folder_or_file_name /home/your_pc_local_folder
```

Для формирования команды экспорта на сервер можно воспользоваться [Google.com](google.com).  
Примерная команда, написанная по памяти:
```bash
scp files_or_folder_from_current_folder cluster_user_name@login1.hpc.spbstu.ru:/home/nilmbb/**user_home**/any_place
```

**WARNING** - для безопасности в коде выше оставлено имя домашней папки **user_home**, её нужно заменить на домашнюю папку текущего пользователя.

## Compile and setup gcdm/gemstat on cluster

**user_home** - папка home текущего пользователя.

Делаем следующие экспорты:

```bash
export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=~/**user_home**/lib:$LD_LIBRARY_PATH
```

Сначала, нужно собрать gemstat, соответсвенно заходим в папку **user_home**/gemstat и выполняем следующие команды:

```bash
autoreconf -fi

./configure --prefix=$HOME

make 

make install
```

Затем переход в папку **user_home**/gcdm и следующие команды:

```bash
autoreconf -fi

CFLAGS="-g -DXTRA -I$HOME/include/libcbgs -DFANN -I/home/nilmbb/**user_home**/include" LIBS="-L$HOME/lib -lcbgs -L/home/nilmbb/kkozlov/lib64 -lfann" .configure --prefix=$HOME

make

make install
```

**WARNING** - для безопасности в коде выше оставлено имя домашней папки **user_home**, её нужно заменить на домашнюю папку текущего пользователя.


На этом, установка gemstat и gcdm закончена.


## GCDM start

Пример команды запуска gcdm:
```bash
./gcdm_printscore -O hes -X ddmrna -H -i 1 -s dde21d -a 1e-2 -g n -Z -x input -p -A ari -r -R gt_mRNA58_new.gd gt_mRNA58_foc_001.ini-deep-output
```
## Deep compile and setup

TBD??

## Deep start

Для запуска **уже установленного** на кластере нужно действовать следующим образом.

1. Выполнить эти 2 экспорта:
```bash
export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/sbin:$LD_LIBRARY_PATH
export PATH=/home/nilmbb/kkozlov/sbin:$PATH
```

2. Для того чтобы дип оставался активным при отключении от кластера, нужно пользоваться утилитой screen - она доступна на кластере (see manuals)[^2] [^3].  
Выполняем команду, чтобы создать screen сессию:
```bash
screen -S "Session name"
```

Внутри сессии выполняем команды запуска Slurm (see manuals)[^4].  

<span style="color:red">**WARNING!: На кластере можно запускать тяжеловесные команды (например, дип) ТОЛЬКО через команды Slurm.**</span>

```bash
srun - N 1 deepmethod --default-name=gt_mRNA58_foc.ini.src
```

Я рекомендую пользоваться дополнительным флагом --comment для того, чтобы задать комментарий к этому процессу.

```bash
srun -N 1 --comment="comment string " deepmethod --default-name=gt_mRNA58_foc.ini.src
```

Теперь, оптимизация через дип запущена. Отключиться от текущей скрин сессии можно хоткеем "ctrl+a d" (опять же смотреть мануалы по скрину) и после этого можно отключаться от кластера, и несмотря на это, внутри скрин-сессии дип останется работать.

При новом подключении к кластеру, следующей командой можно увидеть список существующих скрин-сессий. К любой из них можно подключиться 
и увидеть результаты работы. После завершения работы дипа внутри сессии - сессию надо удалить.

```bash
screen -ls # list of screen sessions
screen -r name_of_session # connect to session by name
```

## Monitoring

Для отслеживания своих задач можно пользоваться командой:
```bash
squeue
```

Она выведет список запущенных задач + задачи в ожидании.  
Лично я пользовался несколько отформатированным выводом:
```bash
squeue —format="%.20i %.10P %j %.10u %T %.10M %.6D %R %S %k"
```
## Additional

**Бессмертные** могут попробовать воспользоваться моими скриптами в папке oldScripts.
Скрипт генерировал входные файлы для дипа, создавал соответствующее количество скрин-сессии и запускал внутри каждой дип.  
Входные файлы лежат в mData.

**WARNING** в моей задаче есть специфика входных аргументов, нестандартные данные были на месте второго аргумента tranlational_transcriptional_delays 
в .gd файле, в моей программе менялась обработка этого аргумента.  


## Manuals  
[^1]: [SCP manuals](https://losst.ru/kopirovanie-fajlov-scp)
[^2]: [Screen manuals](https://www.gnu.org/software/screen/manual/screen.html)
[^3]: [Screen manuals 2](https://losst.ru/komanda-screen-linux)
[^4]: [Slurm manuals](http://icybcluster.org.ua/index.php?lang_id=1&content_id=206)


