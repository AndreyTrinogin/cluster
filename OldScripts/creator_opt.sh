
# $1 - number of optimization, also value for generated deep src files 

if [ -n "$1" ];
then

export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/lib64:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/nilmbb/kkozlov/sbin:$LD_LIBRARY_PATH
export PATH=/home/nilmbb/kkozlov/sbin:$PATH

export PATH=/home/nilmbb/vgursky/atrinogin/bin:$PATH

export LD_LIBRARY_PATH=~/lib:$LD_LIBRARY_PATH

echo SCRIPT_OUPTUT_START


# array=(45 85)
array=(45 85 125 165 205 250 350 450 550 650 750)

# Create dir for opt_num, generate data
	workFolder=opt_"${1}"
	mkdir $workFolder
	echo $workFolder

	for j in ${array[@]};
        do
		cat gt_mRNA58_S_foc.ini.src | sed -e "s/@SLIDE@/$j/g" >  $workFolder/gt_mRNA58_S"${j}"_foc.ini.src
		cat $workFolder/gt_mRNA58_S"${j}"_foc.ini.src | sed -e "s/@SEED@/$RANDOM$RANDOM/g" > $workFolder/gt_mRNA58_S"${j}"_foc_"${1}".ini;
		echo $j;
        done;

	cp gt_mRNA58_new.gd $workFolder
	cp gt_seq_arina_new4.ann $workFolder
	cp gt_seqs.fa $workFolder
	cp gt_tf8.pcm $workFolder

	cd $workFolder

	# start screen sessions, start deeps
	for i in ${array[@]};
	do
		echo $i , $1
		screen -dmS S"${i}"_"${1}"
		screen -S S${i}_${1} -X stuff "echo Session_for_S${i}_${1} ^M"
		screen -S S${i}_${1} -X stuff "source ../exports.sh ^M"
		screen -S S${i}_${1} -X stuff "srun -N 1 --comment="S${i}_${1}" deepmethod --default-name=gt_mRNA58_S"${i}"_foc_"${1}".ini ^M"
	done;
	
screen -dmS control_Screen_"${1}"
screen -S control_Screen_"${1}" -X stuff "echo control ^M"

# screen -S control_Screen_"${1}" -X stuff "./gcdm_printscore -O hes -X ddmrna -H -i 1 -s dde21d -a 1e-2 -g n -Z -x input -p -A ari -r -R gt_mRNA58_new.gd gt_mRNA58_S165_foc_01.ini-deep-output ^M"

cd ../
ls -1

echo SCRIPT_OUTPUT_END

else
	echo "ERROR: num of opt is empty"
fi


# screen -S S${i}_${1} -X stuff "echo $i^M"
# /home/andrey/bin/gt_mRNA58_S165_foc_01.ini-deep-output
